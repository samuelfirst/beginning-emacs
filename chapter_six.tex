\chapter{Chapter Six: Advanced Editing}

\section{Keyboard Macros}
Some editing tasks are repetitive, requiring the same keystrokes to be entered
over and over again.  Keyboard macros provide a way to automate this.  Macros
allow you to bundle a set of keystrokes together and send them with a keyboard
shortcut.

\subsection{Defining and Using Macros}
To begin defining a keyboard macro, type \keys{ctrl+x} \keys{(}.  Then enter
the keystrokes that you want to record.  These keystrokes can be anything
other than \keys{ctrl+g}, which will stop the macro definition.  Once the
macro is defined, enter \keys{ctrl+x} \keys{)} to stop defining the macro.
You can then run the macro with \keys{ctrl+x} \keys{e}.  If the repetitive
task spans multiple evenly spaced lines, a good practice is to move the cursor
into position to edit the next line before ending the macro definition.  This
is so you don't have to adjust the cursor each time you run the macro.  This
is especially effective when considering that you can run a macro again by
just pressing \keys{e}.

\subsection{Macro Query}
Sometimes you will want the ability to prematurely exit a macro, depending on
the conditions.  If you press \keys{ctrl+x} \keys{q} while defining a macro,
it will insert a query into the macro at the current point (it won't actually
do anything until the macro is run).  Then when the macro is executed, upon
reaching that point, it will give the prompt
\textcolor{olive}{Proceed with macro? (Y, N, RET, C-l, C-r)}.  To continue
with the prompt, press \keys{y}; to exit at the prompt, press \keys{n}.

To edit the file before continuing the macro, type \keys{ctrl+r}.  This enters
recursive edit mode.  To exit recursive edit mode, press \keys{ctrl+alt+c}.
If you want to enter recursive edit mode without the prompt, enter
\keys{ctrl+u+x} \keys{q} while defining the macro.  It should be noted that
this will also enter recursive edit mode during the definition period.  To
go back to defining the macro, press \keys{ctrl+alt+c}.

\subsection{Macro Counter}
If your repetitive task involves counting, Emacs has a counter built in to its
macro system that increments each time a macro runs.  To insert the value of the
counter into the current buffer, enter \keys{ctrl+x+k+i}.  This, as well as
the following shortcuts, can be used both inside and outside of macro
definition.  If you want to set the counter to an arbitrary number, type
\keys{ctrl+x+k+c}, and enter the number you want to set it to at the prompt.
To add an arbitrary value to the counter, type \keys{ctrl+x+k+a}.  If you want
to apply a different format to the counter, press \keys{ctrl+x+k+f} and enter
the new format at the prompt.  In the new format, the number is represented by
'\%d'.  For example, 'Hello World! \%d' would become 'Hello World [counter]'
on each run.

\subsection{Editing Macros}
To append more instructions onto a keyboard macro, use the shortcut
\keys{ctrl+u+x} \keys{(}.  This will run the macro, then allow you to enter
more keystrokes into it.  To close the macro when you are done, type
\keys{ctrl+x} \keys{)}.  Now when you run the macro, it will also run the
instructions you added.

If you want to insert new instructions in the middle of the macro, type
\keys{ctrl+x+k} \keys{space} to enter stepwise editing mode.  Below is a table
of the stepwise editing mode commands.

\begin{table}[H]
    \caption{Stepwise Editing Commands}
    \centering
    \begin{tabular}{| rp{1\linewidth} | lp{1\linewidth} |}
      \hline
      \multicolumn{1}{| c }{\textbf{Command}} &
      \multicolumn{1}{c |}{\textbf{What it does}} \\
      \hline
      \keys{y}, \keys{space} \vline & inserts current character and advance to next
      character\\
      \keys{n}, \keys{d}, \keys{delete} \vline & removes current character from the
      macro\\
      \keys{f} \vline & skips current character, but does not remove it from the
      macro\\
      \keys{tab} \vline & insert a series of similar characters.\footnotemark[1]\\
      \keys{c} \vline & executes macro from the current point, and exits stepwise
      editing mode\\
      \keys{ctrl+k} \vline & skips and delets the rest of the macro\\
      \keys{q}, \keys{ctrl+g} \vline & cancels the editing and discards changes\\
      \keys{i}  \vline & inserts new keypresses; press \keys{ctrl+g} to stop
      inserting\\
      \keys{I} \vline & inserts a single new character\\
      \keys{r} \vline & replaces a character with a series of new keypresses; press
      \keys{ctrl+g} to stop inserting\\
      \keys{R} \vline & replaces the current character with a single new character\\
      \keys{a} \vline & inserts current character, then inserts new
      keypresses; press \keys{ctrl+g} to stop inserting\\
      \keys{A} \vline & executes the rest of the macro, then inserts new keypresses;
      press \keys{ctrl+g} to stop inserting\\
      \hline
    \end{tabular}
\end{table}

\subsection{Saving Macros}
Sometimes when editing, you might want to set aside macros for future use, or
use multiple macros at once.  Emacs allows for this through the ability to
save and name macros.

\subsubsection{Temporarily}
There are two methods of saving a macro for the current section: as a command,
and/or bound to a keyboard shortcut.  To save it as a command, type
\keys{ctrl+x+k} \keys{n}.  Then, enter the command you want to bind it to.
This can be anything that is not already a command (it can however, be a
keyboard macro bound to a command).  It may contain special characters and
whitespace, however, it may not work correctly if it's entirely whitespace.
To bind it to a keyboard shortcut, type \keys{ctrl+x+k} \keys{b}, then enter
the keyboard shortcut that you want to bind it to.  If you enter a shortcut
that is already used by another command, Emacs will ask whether you want to
overwrite it.  Enter \textcolor{blue}{yes} to overwrite, or
\textcolor{blue}{no} to not at the prompt.

\subsubsection{Permanently}
To permanently save a keyboard shortcut, first bind it to a command with
\keys{ctrl+x+k} \keys{n}.  Then open the file $\sim$/.emacs (\keys{ctrl+x+f} 
\textcolor{blue}{$\sim$/.emacs}), type \keys{alt+x}, then enter the command
\textcolor{blue}{insert-kbd-macro} and press \keys{enter}.  This should insert
the emacs-lisp equivalent of the macro into the file (see the figure below for
an example of what this looks like).  Now, the next time you start Emacs, the
macro will still be bound to the command, however, any keystrokes that are
bound to it will not be carried over to future sections.

\begin{figure}[H]
\caption{Permanently Saved Keyboard Macro Example}
\begin{verbatim}
(fset 'macro-name
      (lambda (&optional arg) "Keyboard macro." (interactive "p")
        (kmacro-exec-ring-item (quote ("macro keystrokes" 0 "%d")) arg)))
\end{verbatim}
\end{figure}

\section{Regular Expressions}
In chapter three we used regular expressions to perform generic search and
replace operations.  Regular expressions are patterns that describe how
strings are composed.  To see the commands that use regular expressions, refer
to the searching and replacing sections of chapter three (there are also some
at the end of this section).

\subsection{How They Work}
Regular expressions use special characters to describe patterns.  The table
below contains a description of the basic special characters.

\begin{table}[H]
    \caption{Regular Expressions: Basic Special
      Characters\cite{emacs-wiki-regex-syntax}}
    \centering
    \begin{tabular}{| l | l |}
      \hline
      \multicolumn{1}{| c |}{\textbf{Character(s)}} &
      \multicolumn{1}{c |}{\textbf{Meaning}} \\
      \hline
      .               & any non-newline character                     \\
      \**             & previous char/group repeated 0 or more times  \\
      +               & previous char/group repeated 1 or more times  \\
      ?               & previous char/group repeated 0 or 1 times     \\
      \^{}            & start of line                                 \\
      \$              & end of line                                   \\
      {[} and {]}     & any character between the brackets            \\
      {[}\^{} and {]} & any character not between the brackets        \\
      \textbackslash  & prevents interpretation of next character     \\
      \hline
    \end{tabular}
\end{table}

By combining these characters, it is possible to build regular expression
patterns.  For example, the pattern to find any string between angled
brackets, would be \texttt{<.+>}.  In this expression, the first character,
\texttt{<}, is not a special character, so it's interpreted literally.  The
second character, \texttt{.}, is a special character, meaning any single
character, so the pattern so far will recognize angled brackets followed by
any character.  The period, however, is followed by a \texttt{+} which means
that the \texttt{.} is repeated 1 or more times.  The pattern at this point
will recognize angled brackets followed by any number of characters up until a
new line.  After adding the last character \texttt{>}, the pattern will match
text that starts with an opening angled bracket, has any character except new
lines in it, and ends with a closing angled bracket.

If the expression had started with a \texttt{\^} (\texttt{\^{}<.+>}), it would
only match angled brackets at the beginning of the line.  If it had started
with \texttt{\^} and ended with \texttt{\$} (\texttt{\^{}<.+>\$}), it would
only match lines that begin and end with angled brackets.  If the expression
had been \texttt{\^{}[<\{].+[>\}]\$}, it would match curly brackets as well as
angled brackets, meaning both \texttt{<hello world>} and \texttt{\{hello
  world\}} would match the pattern.  Including square brackets in the pattern
is a bit more complicated, and requires some of the more complex special
characters in the table below.

\begin{table}[H]
    \caption{Regular Expressions: Complex Special
      Characters\cite{emacs-wiki-regex-syntax}}
    \centering
    \begin{tabular}{| l | l |}
      \hline
      \multicolumn{1}{| c |}{\textbf{Character(s)}} &
      \multicolumn{1}{c |}{\textbf{Meaning}} \\
      \hline
      \textbackslash$\vert$ & or                                             \\
      \textbackslash w      & word constituent                               \\
      \textbackslash b      & word boundary                                  \\
      \textbackslash(       & start of group                                 \\
      \textbackslash)       & end of group                                   \\
      \textbackslash$<$     & start of word                                  \\
      \textbackslash$>$     & end of word                                    \\
      \textbackslash\_$<$   & start of symbol                                \\
      \textbackslash\_$>$   & end of symbol                                  \\
      \textbackslash\`      & start of buffer/string                         \\
      \textbackslash'       & end of buffer/string                           \\
      \textbackslash n      & string matched by the nth group                \\
      \textbackslash\{n\}   & previous char/group repeated n times           \\
      \textbackslash\{n,N\} & previous char/group repeated from n to N times \\
      \textbackslash\{n,\}  & previous char/group repeated at least n times  \\
      \textbackslash=       & located at point                               \\
      \hline
    \end{tabular}
\end{table}

Using these, we can construct the expression
\texttt{\^{}\textbackslash([<\{]\textbackslash$\vert$\textbackslash[\textbackslash)+.+\textbackslash([>\}]\textbackslash$\vert$\textbackslash]\textbackslash)+\$}.
This looks a bit intimidating at first, but once broken down, it's fairly
straight forward.  The \texttt{\^} serves the same purpose as above: to match
anything that starts at the beginning of the line.  The next characters,
\textbackslash( start a group.  A group is like a mini-pattern within the
expression.  The first part of the group, \texttt{[<\{]}, will match either $<$
or \{, so the overall expression will match any line beginning with one of
those characters.  The next part of the group is \textbackslash$\vert$, which
means the group can match either the previous item or the next item,
\textbackslash[.  The \textbackslash escapes the special character [, telling
Emacs to interpret it literally.  Then, the group is closed with
\texttt{\textbackslash)}.  The \texttt{+} after the group means that it will
search for one or more characters that match, so the expression will now match
any line that begins with $<$, \{, or [.  The \texttt{.+} will still match every
character up to a new line.  The second group,
\texttt{\textbackslash([>\}]\textbackslash$\vert$\textbackslash]\textbackslash)+},
is the reverse of the previous group, meaning it will match $>$, \}, or ].  The
\texttt{\$} means that the line must end after the closing bracket.  The
expression will now match \texttt{<hello world>}, \texttt{\{hello world\}},
and \texttt{[hello world]}.

\subsubsection{Greedy vs. Lazy}
Say we have a regex query that searches for any text inside quotes.  If we
run the query over the line \texttt{"Lorem ipsum "dolor sit" amet"}.  Should
it return a match for the whole line, or two matches (\texttt{"Lorem ipsum "}
and \texttt{" amet"}).  The default behavior for this is to match as far as
possible, meaning that by default Emacs will return the whole line as a
match.  This is referred to as greedy, because it consumes as much as
possible.  Lazy regular expressions are the opposite of this: they consume as
little as possible.  The lazy versions of \texttt{*}, \texttt{+}, and
\texttt{?} are \texttt{*?}, \texttt{+?}, and \texttt{??}.  The figure below
shows both the greedy and lazy versions the expression described.

\begin{figure}[H]
\caption{Greedy and Lazy Regular Expressions}
\begin{verbatim}
Greedy: ".*"
  Lazy: ".*?"
\end{verbatim}
\end{figure}

\subsection{Categories of Characters}
You can also refer to characters by category in regular expression queries.
Categories are ascii, latin, greek, and non-ascii.  Ascii, latin, and greek
encompass english language characters, however, latin also covers
non-punctuation characters and characters with diacritical marks.  The greek
category contains some symbols, and the greek alphabet.  If you want to see
what category a character is in, move the cursor over it, and press
\keys{ctrl+u+x} \keys{=}.  The category the character is in will be to the
right of \texttt{script:}.  Refer to the table below for the regular
expression equivalents of the categories.

\begin{table}[H]
  \caption{Regular Expressions: Character
    Categories\cite{emacs-wiki-regex-syntax}}
  \centering
  \begin{tabular}{| l | l |}
    \hline
    \multicolumn{1}{| c |}{\textbf{Syntax Class}} &
    \multicolumn{1}{c |}{\textbf{Meaning}} \\
    \hline
    \textbackslash ca & ascii character     \\
    \textbackslash Ca & non-ascii character \\
    \textbackslash cl & latin character     \\
    \textbackslash cg & greek character     \\
    \hline
  \end{tabular}
\end{table}

%% This is temporary; I'll figure out a better way to show hyperlinks
%% later.
You can find the full 
\textcolor{blue}{\underline{
    \href{https://en.wikipedia.org/wiki/ASCII\#Character\_set}{ascii}}},
\textcolor{blue}{\underline{
    \href{https://en.wikipedia.org/wiki/ISO/IEC\_8859-1\#Code\_page\_layout}
    {latin}}},
and \textcolor{blue}{\underline{
  \href{https://en.wikipedia.org/wiki/ISO/IEC\_8859-7\#Codepage\_layout}
       {greek}}}
character sets on their wikipedia pages.

\subsection{Syntax Classes}
Emacs also accepts a description of the type character you want, or syntax
class.  There are two kinds of syntax classes: bracketed and unbracketed; the
sections below cover how to use them.

\subsubsection{Unbracketed}
Unbracketed syntax classes are mainly programming focussed, providing ways to
specify comments, string/character quotes, and other programming constructs.
It automatically applies these for whatever language you are editing.

\begin{table}[H]
  \caption{Regular Expressions: Syntax Classes
    (Unbracketed)\cite{emacs-wiki-regex-syntax}}
  \centering
  \begin{tabular}{| l | l |}
    \hline
    \multicolumn{1}{| c |}{\textbf{Syntax Class}} &
    \multicolumn{1}{c |}{\textbf{Meaning}} \\
    \hline
    \textbackslash s-              & Whitespace                \\
    \textbackslash sw              & Word Constituent          \\
    \textbackslash s\_             & Symbol Constituent        \\
    \textbackslash s.              & Punctuation               \\
    \textbackslash s(              & Open Delimiter            \\
    \textbackslash s)              & Close Delimiter           \\
    \textbackslash s"              & String Quote              \\
    \textbackslash s\textbackslash & Escape Character          \\
    \textbackslash s/              & Char Quote Character      \\
    \textbackslash s\$             & Paired Delimiter          \\
    \textbackslash s'              & Expression Prefix         \\
    \textbackslash s$<$            & Comment Start             \\
    \textbackslash s$>$            & Comment End               \\
    \textbackslash s!              & Generic Comment Delimiter \\
    \textbackslash s$\vert$        & Generic String Delimiter  \\
    \hline
  \end{tabular}
\end{table}

\subsubsection{Bracketed}
Bracketed syntax classes work in a way that is not immediately obvious.  They
must be surrounded by two sets of brackets (ex. \texttt{[[:alpha:]]}),
otherwise they produce odd and buggy behavior.  Other characters can be put in
the outside brackets, like \texttt{[!@\#\$[:alpha:]]}.  You can also include
more than one bracketed syntax class per outer bracket:
\texttt{[[:alpha:][:digit:]]}.

\begin{table}[H]
  \caption{Regular Expressions: Syntax Classes
    (Bracketed)\cite{emacs-wiki-regex-syntax}}
  \centering
  \begin{tabular}{| l | l |}
    \hline
    \multicolumn{1}{| c |}{\textbf{Syntax Class}} &
    \multicolumn{1}{c |}{\textbf{Meaning}} \\
    \hline
    \texttt{[:digit:]}  & A number                     \\
    \texttt{[:alpha:]}  & A letter                     \\
    \texttt{[:alnum:]}  & A letter or number           \\
    \texttt{[:upper:]}  & A capital letter             \\
    \texttt{[:lower:]}  & A lowercase letter           \\
    \texttt{[:graph:]}  & A visible character          \\
    \texttt{[:print:]}  & A visible character or space \\
    \texttt{[:space:]}  & A whitespace character       \\
    \texttt{[:blank:]}  & A space or tab               \\
    \texttt{[:ascii:]}  & An ascii character           \\
    \texttt{[:cntrl:]}  & A control character          \\
    \texttt{[:xdigit:]} & A hexadecimal digit          \\
    \hline
  \end{tabular}
\end{table}

\subsection{Examples}
\begin{list}{\labelitemi}{\leftmargin=1em}
\item Web address:
\texttt{
\textbackslash(http[s]?://\textbackslash)?w+++\textbackslash..*\textbackslash..*}
\item ipv4 address:
\texttt{
\textbackslash([12345]???\textbackslash.\textbackslash)+++\textbackslash.[12345]???}
\item Email address: \texttt{.*@.*\textbackslash..*}
\end{list}

\subsection{Commands That Use Regex}
The table below contains the commands that use regular expressions.  The
commands marked with an asterisk support extended regular expressions.

\begin{table}[H]
  \caption{Regular Expressions: Character
    Categories\cite{emacs-wiki-regex-commands}}
  \centering
  \begin{tabular}{| l | l |}
    \hline
    \multicolumn{1}{| c |}{\textbf{Command}} &
    \multicolumn{1}{c |}{\textbf{Purpose}} \\
    \hline
    isearch-forward-rexexp  & incremental forward regex search\\
    isearch-backward-regexp & incremental backward regex search\\
    replace-regexp          & replaces regex string\\
    query-replace-regexp    & replaces regex string, but prompt before\\
    align-regexp            & moves everything matching to one tab level\\
    highlight-regexp        & colors matching lines\\
    occur                   & shows matches in new buffer\\
    multi-occur             & shows matches from all buffers\\
    how-many                & counts number of occurences forward from cursor\\
    keep-lines              & deletes non-matching lines\\
    flush-lines             & deletes matching lines\\
    $grep^{*}$               & Runs grep command (must specify file)\\
    lgrep                   & Grep command without flags\\
    rgrep                   & Like lgrep, but recursive\\
    dired-do-copy-regexp    & copy files with matching names\\
    dired-do-rename-regexp  & rename files with matching names\\
    find-grep-dired         & Search dired with grep\\
    \hline
  \end{tabular}
\end{table}
$^{*}$The difference between normal and extended regular expressions is that
?, +, \{, \}, $\vert$, (, and ) need to be preceded by backslashes in normal
regular expressions.  To use extended regular expressions, use the -E flag
instead of the -e flag.

\section{Rectangle Editing}

\subsection{Selecting}

\subsection{Editing Rectangles}

\section{Programming Tips}

\subsection{Electric Modes}

\subsection{Glasses Mode}

\subsection{C Warn Mode}

\subsection{Linum Mode}

\subsection{Viper Mode}

\subsection{Sub/Super Word Modes}

\section{Practice}

\subsection{Macros}

\subsubsection{Writing Lines}

\subsection{Regular Expressions}

\subsection{Rectangle Editing}

\section{Something Fun}

\subsection{Pong, Tetris, and Snake}
Emacs includes three classic games: pong, tetris, and snake.  See below to
find out how to play them.

\begin{figure}[H]
  \caption{Pong, Tetris, and Snake Example}
  \includegraphics[width=1\linewidth]{include/images/pong-tetris-snake-example}
\end{figure}

\begin{enumerate}
\item Pong
  \begin{enumerate}
  \item Type \keys{alt+x}
  \item Type \textcolor{blue}{pong} and press \keys{enter}
  \item Controls:
    \begin{enumerate}
    \item Move Right Paddle: \keys{$\uparrow$}/\keys{$\downarrow$} or
      \keys{8}/\keys{2}
    \item Move Left Paddle: \keys{$\leftarrow$}/\keys{$\rightarrow$} or
      \keys{4}/\keys{6}
    \item Pause: \keys{p}
    \item Quit: \keys{q}
    \end{enumerate}
  \item NOTE: There is no single player mode for this game
  \end{enumerate}
\item Tetris
  \begin{enumerate}
  \item Type \keys{alt+x}
  \item Type \textcolor{blue}{tetris} and press \keys{enter}
  \item Controls:
    \begin{enumerate}
    \item Rotate Block: \keys{$\uparrow$}
    \item Move Block Down: \keys{$\downarrow$}
    \item Move Block Left: \keys{$\leftarrow$}
    \item Move Block Right: \keys{$\rightarrow$}
    \item Pause: \keys{p}
    \item Quit: \keys{q}
    \end{enumerate}
  \end{enumerate}
\item Snake
    \begin{enumerate}
  \item Type \keys{alt+x}
  \item Type \textcolor{blue}{snake} and press \keys{enter}
  \item Controls:
    \begin{enumerate}
    \item Set Direction up: \keys{$\uparrow$}
    \item Set Direction Down: \keys{$\downarrow$}
    \item Set Direction Left: \keys{$\leftarrow$}
    \item Set Direction Right: \keys{$\rightarrow$}
    \item Pause: \keys{p}
    \item Quit: \keys{q}
    \end{enumerate}
  \end{enumerate}
\end{enumerate}

\footnotetext[0]{All shortcuts and commands can be found in the Emacs
  manual\cite{Emacs-Manual}}

\footnotetext[1]{This means that if the macro contains a series of
  alphanumeric characters/symbols, pressing tab will insert that series of
  characters until it encounters a character that does not match (tab, return,
  backspace, delete, arrow keys).  Similarly, if it encounters a series of one
  of the non-matching characters above, it will insert it until it encounters
  a different character.}
