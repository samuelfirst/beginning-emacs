* Tables
#+NAME: position-salary-table
| Name            | Position         | Salary ($US) |
|-----------------+------------------+--------------|
| J Random Worker | CFO              |       300000 |
| Fred FooBar     | CEO              |       600000 |
| Alice           | CTO              |       200000 |
| Bob             | Sales Assosciate |        18000 |
| Carol           | Manager          |        73000 |
| John Doe        | Sales Assosciate |        15000 |
| Jane Doe        | Sales Assosciate |        16000 |
* Code
