\chapter{Chapter One: Basic Editing}
% Consider fleshing this section out
To use any text editor you need to be able to do three things: open files,
modify them, and save them when you're done.  When using Emacs this is
accomplished through keyboard shortcuts.  The goal of this chapter is to teach
you the basic shortcuts you will need when editing text.

%==================================================================================%

\section{Opening Files}
The shortcut for opening files is \keys{\ctrl + x + f}.  To
use it, hold down the control key with your pinky, press the x key with your
index finger, and, without lifting your pinky from the control key, press the
f key.  If this is uncomfortable, shift your hand slightly off the home row,
so that when your pinky rests on the control key, your index finger will rest
on the x key (Shown in the picture below).  Once you have pressed the keys, if
you look at the bottom of the window, you will notice a line of text along the
lines of \textcolor{olive}{Find file: \textasciitilde/}.  To open a file,
simply type its path and press enter (for example, if I wanted to open the
file list.txt in the Documents directory, I would type Documents/list.txt and
press enter).  If the file you are trying to open does not exist, Emacs will
create the file for you.

\begin{figure}[H]
  \caption{Hand Placement Diagram}
  \includegraphics[width=1\linewidth]{include/images/hand_placement}
\end{figure}

\subsection{Tab Completion}
Typing the entire path to a file is rather unpleasant, which is why Emacs has
implemented a method to save time and effort when opening a file: tab
completion.  When typing the name of the path to your file, rather than typing
the whole thing, type a few characters and press \keys{tab}.  It
should fill in the rest of the name of that part of the path for you (if it
doesn't, it is because you have multiple directories starting with the
characters you typed, and there isn't enough information to determine
which one you want; try typing a few more characters, then press tab again).

You can also use tab completion to list the files in a directory by pressing
\keys{tab} twice.  This can be useful if you don't remember the
exact path to a file.

%==================================================================================%

\section{Modifying Files}
Editing files in Emacs is similar to editing files in other text editors.  You
use the arrow keys to navigate, and the keyboard to type.  There are however,
a few differences that can be tricky for a new user.  This section will
explain what those differences are and why they are that way.

\subsection{Selecting Text}
To select text in emacs, hold \keys{shift} and press either the
left or the right arrow key.  This will select the character to the left or
the right of the cursor.  If you continue to hold shift and press the arrow
key, it will highlight the next character over.  Pressing the arrow key in the
opposite direction will unhighlight the character.  This is rather tedious,
and so Emacs has a way to expedite the process.  If you hold down
\keys{\ctrl} while pressing shift and one of the arrow keys, it will
select an entire word.  If you do it with the up or down arrow keys, it will
select an entire section.

\subsection{Copying, Cutting and Pasting}
Emacs is old, older than the terminology 'copy', 'cut', and 'paste' and older
than the standard CTRL\hyp{}C, CTRL\hyp{}X, CTRL\hyp{}V keyboard shortcuts.
As such, it uses a completely different terminology and keyboard shortcuts. In
Emacs, the clipboard is called the kill-ring, and copying is called saving to
the kill-ring.  Its shortcut is \keys{alt + w}.  Similarly,
cutting a region of text is called killing the region (the shortcut is
\keys{\ctrl + w}), and pasting is called yanking (because you are
yanking text from the kill ring).  The shortcut to yank is
\keys{\ctrl + y}.  This might seem a bit confusing, or hard to
get used to at first, however, with a bit of practice, the adjustment is
fairly easy.

\subsection{Undoing}
In most programs, CTRL\hyp{}Z is used to undo the last action (if for example,
you deleted some text by mistake, you would press CTRL\hyp{}Z to undo the
deletion).  In Emacs, however, the undo shortcut is
\keys{\ctrl + x} \keys{u}.  It should be noted that this is
\textcolor{red}{NOT} the same
thing as CTRL + x + u.  Rather than holding the control key while you
press the x and u keys, only hold control while pressing the x key; then
release the control and x keys and press the u key.  For this shortcut, you
should be using both hands: the left hand to press the control and x keys, and
the right to press the u key.

\subsection{Search and Replace}
Searching for and replacing text are important features when editing large
files.  In Emacs, you can search forward (everything below the cursor) with
\keys{\ctrl + s} and backward (everything above the cursor) with
\keys{\ctrl + r}, then typing the text you want to search for.
You can then search for the next instance of the text by pressing
\keys{\ctrl + s} or \keys{\ctrl + r} again.

To replace text, type \keys{alt + x}, type replace-string, and
press enter. Now type the text that you want to replace, press enter, type the
text that you want to replace it with, and press enter again.

\subsection{Common Pitfalls}
While most of Emacs is fairly straight-forward, there are a few things that
tend to trip up new users.  Below, I've listed some of the more common ones,
and what to do if you encounter them.

\subsubsection{Entering Overwrite Mode}
When reaching for the delete key, you might accidentally hit the insert key,
which will put Emacs into overwrite mode.  In overwrite mode, text in front of
the cursor will be overwritten by what you type.  If you encounter this, just
press the insert key again to exit overwrite mode.

\subsubsection{CTRL\hyp{}X\hyp{}U}
When using undo, be careful to not hold down control while pressing u,
otherwise, Emacs will prompt asking whether to enable the upcase region
command.  After this if you attempt to continue undoing, Emacs will redo what
you previously undid.  To force Emacs to continue undoing where you left off,
press \keys{alt + x}, type \textcolor{blue}{undo\hyp{}only}, and press enter.

\subsubsection{Accidentally Minimizing Emacs}
Occasionally, when attempting to press CTRL + x, you might accidentally
press CTRL + z.  This will minimize Emacs, making it seem like it closed
without giving you a chance to save your work.  If this happens, don't panic,
just unminimize Emacs, and continue where you left off.

\subsubsection{Accidentally Typing ALT + x}
If you accidentally type ALT + x, emacs will wait for you to give it a
command.  To go back to the file you were editing, press
\keys{\ctrl + g} or press \keys{esc esc esc}.

\subsection{Editing Code}
Emacs is fantastic for programming, however, it does some things that might
seem odd if you are transitioning from another text editor or IDE.  The
largest difference is how Emacs handles auto-tabbing.  In Emacs, you can
adjust the indentation of a line by pressing tab anywhere in the line.  If a
line of code is indented to far, keep pressing tab until it is in the proper
location.

%==================================================================================%

\section{Saving Files}
Once you have finished editing a file, you have to save it. In order to save
the current file, press \keys{\ctrl + x + s}.  To save all the
files you have open press \keys{\ctrl + x} \keys{s}.  To save the
current file under a different name, press
\keys{\ctrl + x + w}, type the new name you want to save it
under, and press enter.

%==================================================================================%

\section{Exiting Emacs}
To exit Emacs, press \keys{\ctrl + x + c}.  If you have unsaved
files, it will ask you whether you want to save them before exiting.

%==================================================================================%

\section{Practice}
Now that you know some of the shortcuts and commands used to edit files in
Emacs, it is important that you practice them in order to commit them to
muscle memory.

\subsection{Practice One: Grocery List}
For this practice exercise type up a grocery list, making sure to save after
each line.

% Add more practice exercises

%==================================================================================%

\section{Something Fun}
All work and no play makes Jack a dull boy, so at the end of each chapter, I'm
going to show you how to do something fun in Emacs.

\subsection{Converting Text to Morse Code}
\begin{list}{\labelitemi}{\leftmargin=1em}
\item Step 1: Open the file morse.txt in the beginning-emacs/include/chapter\_one
directory.

\item Step 2: Select some of the text.

\item Step 3: Press \keys{alt + x}.

\item Step 4: Type \textcolor{blue}{morse-region} and press enter.
\end{list}

To change it back repeat steps one through three, type unmorse-region, and
press enter.

\footnotetext{All shortcuts and commands can be found in the Emacs
  manual\cite{Emacs-Manual}}
