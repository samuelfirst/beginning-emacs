\contentsline {chapter}{\numberline {1}Chapter One: Basic Editing}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Opening Files}{1}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Tab Completion}{2}{subsection.1.1.1}
\contentsline {section}{\numberline {1.2}Modifying Files}{2}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Selecting Text}{2}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Copying, Cutting and Pasting}{2}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Undoing}{3}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}Search and Replace}{3}{subsection.1.2.4}
\contentsline {subsection}{\numberline {1.2.5}Common Pitfalls}{3}{subsection.1.2.5}
\contentsline {subsubsection}{Entering Overwrite Mode}{3}{section*.3}
\contentsline {subsubsection}{CTRL\hyp {}X\hyp {}U}{3}{section*.4}
\contentsline {subsubsection}{Accidentally Minimizing Emacs}{3}{section*.5}
\contentsline {subsubsection}{Accidentally Typing ALT + x}{4}{section*.6}
\contentsline {subsection}{\numberline {1.2.6}Editing Code}{4}{subsection.1.2.6}
\contentsline {section}{\numberline {1.3}Saving Files}{4}{section.1.3}
\contentsline {section}{\numberline {1.4}Exiting Emacs}{4}{section.1.4}
\contentsline {section}{\numberline {1.5}Practice}{4}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}Practice One: Grocery List}{4}{subsection.1.5.1}
\contentsline {section}{\numberline {1.6}Something Fun}{4}{section.1.6}
\contentsline {subsection}{\numberline {1.6.1}Converting Text to Morse Code}{5}{subsection.1.6.1}
\contentsline {chapter}{Preamble}{1}{chapter*.2}
\contentsline {chapter}{\numberline {2}Chapter Two: Buffers and Frames}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}Buffers}{6}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Switching Buffers}{6}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Closing Buffers}{6}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}The Mini-Buffer}{7}{subsection.2.1.3}
\contentsline {section}{\numberline {2.2}Frames}{8}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Opening Frames}{8}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Switching Frames}{9}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Closing Frames}{9}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}Practice}{9}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Practice One: Transcribing}{9}{subsection.2.3.1}
\contentsline {section}{\numberline {2.4}Something Fun}{9}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Converting Text to the Nato Phonetic Alphabet}{10}{subsection.2.4.1}
\contentsline {chapter}{\numberline {3}Chapter Three: \keys {alt + x} and Basic Commands}{11}{chapter.3}
\contentsline {section}{\numberline {3.1}Commands vs. Shortcuts}{11}{section.3.1}
\contentsline {section}{\numberline {3.2}Common Shortcuts and Their Commands}{12}{section.3.2}
\contentsline {section}{\numberline {3.3}Entering Commands}{12}{section.3.3}
\contentsline {section}{\numberline {3.4}Why Emacs Shows M\hyp {}x in the Mini-Buffer}{13}{section.3.4}
\contentsline {section}{\numberline {3.5}Some Useful Commands}{13}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Searching}{14}{subsection.3.5.1}
\contentsline {subsubsection}{The Different Kinds of Search}{14}{section*.7}
\contentsline {subsection}{\numberline {3.5.2}Replacing}{14}{subsection.3.5.2}
\contentsline {subsubsection}{The Different Kinds of Replace}{14}{section*.8}
\contentsline {subsection}{\numberline {3.5.3}Spell Check}{15}{subsection.3.5.3}
\contentsline {subsubsection}{Ispell}{15}{section*.9}
\contentsline {subsubsection}{Flyspell}{15}{section*.10}
\contentsline {section}{\numberline {3.6}Practice}{16}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}Search and Destroy}{16}{subsection.3.6.1}
\contentsline {subsection}{\numberline {3.6.2}Spelling Counts}{16}{subsection.3.6.2}
\contentsline {section}{\numberline {3.7}Something Fun}{16}{section.3.7}
\contentsline {subsection}{\numberline {3.7.1}M-x Butterfly}{16}{subsection.3.7.1}
\contentsline {chapter}{\numberline {4}Chapter Four: Understanding Emacs Conventions and Quirks}{17}{chapter.4}
\contentsline {section}{\numberline {4.1}Alternate Key Names and Shortcut Conventions}{17}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Super and Meta}{17}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}C/S/M-[char]}{17}{subsection.4.1.2}
\contentsline {section}{\numberline {4.2}Keyboard Driven Interface}{18}{section.4.2}
\contentsline {section}{\numberline {4.3}Something Fun}{18}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Conway's Game of Life}{18}{subsection.4.3.1}
\contentsline {chapter}{\numberline {5}Chapter Five: Modes}{19}{chapter.5}
\contentsline {section}{\numberline {5.1}What are Modes}{19}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Major vs. Minor}{19}{subsection.5.1.1}
\contentsline {section}{\numberline {5.2}Some Useful Modes}{20}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Org-mode}{20}{subsection.5.2.1}
\contentsline {subsubsection}{Starting Org-mode}{21}{section*.11}
\contentsline {subsubsection}{Lists}{21}{section*.12}
\contentsline {subsubsection}{Clocking in and out}{21}{section*.13}
\contentsline {subsubsection}{Tables}{21}{section*.14}
\contentsline {subsubsection}{Hyperlinks}{22}{section*.15}
\contentsline {subsubsection}{In-Line Images}{22}{section*.16}
\contentsline {subsubsection}{Code Blocks}{22}{section*.17}
\contentsline {subsubsection}{Agenda}{25}{section*.18}
\contentsline {subsubsection}{Exporting}{26}{section*.19}
\contentsline {subsection}{\numberline {5.2.2}Scheduling}{27}{subsection.5.2.2}
\contentsline {subsubsection}{Calendar}{27}{section*.20}
\contentsline {subsubsection}{Diary}{28}{section*.21}
\contentsline {subsubsection}{Org-mode Integration}{30}{section*.22}
\contentsline {subsection}{\numberline {5.2.3}Image Mode}{30}{subsection.5.2.3}
\contentsline {section}{\numberline {5.3}Practice}{30}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Org Basics}{30}{subsection.5.3.1}
\contentsline {subsubsection}{Bonus: Programming in org}{30}{section*.23}
\contentsline {subsection}{\numberline {5.3.2}Remember, Remember}{30}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Bonus: Researching Other Modes}{30}{subsection.5.3.3}
\contentsline {section}{\numberline {5.4}Something Fun}{31}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Zone Mode}{31}{subsection.5.4.1}
\contentsline {chapter}{\numberline {6}Chapter Six: Advanced Editing}{32}{chapter.6}
\contentsline {section}{\numberline {6.1}Keyboard Macros}{32}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Defining and Using Macros}{32}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Macro Query}{32}{subsection.6.1.2}
\contentsline {subsection}{\numberline {6.1.3}Macro Counter}{33}{subsection.6.1.3}
\contentsline {subsection}{\numberline {6.1.4}Editing Macros}{33}{subsection.6.1.4}
\contentsline {subsection}{\numberline {6.1.5}Saving Macros}{34}{subsection.6.1.5}
\contentsline {subsubsection}{Temporarily}{34}{section*.24}
\contentsline {subsubsection}{Permanently}{34}{section*.25}
\contentsline {section}{\numberline {6.2}Regular Expressions}{35}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}How They Work}{35}{subsection.6.2.1}
\contentsline {subsubsection}{Greedy vs. Lazy}{37}{section*.26}
\contentsline {subsection}{\numberline {6.2.2}Categories of Characters}{37}{subsection.6.2.2}
\contentsline {subsection}{\numberline {6.2.3}Syntax Classes}{37}{subsection.6.2.3}
\contentsline {subsubsection}{Unbracketed}{38}{section*.27}
\contentsline {subsubsection}{Bracketed}{38}{section*.28}
\contentsline {subsection}{\numberline {6.2.4}Examples}{39}{subsection.6.2.4}
\contentsline {subsection}{\numberline {6.2.5}Commands That Use Regex}{39}{subsection.6.2.5}
\contentsline {section}{\numberline {6.3}Rectangle Editing}{41}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Selecting}{41}{subsection.6.3.1}
\contentsline {subsection}{\numberline {6.3.2}Editing Rectangles}{41}{subsection.6.3.2}
\contentsline {section}{\numberline {6.4}Programming Tips}{41}{section.6.4}
\contentsline {subsection}{\numberline {6.4.1}Electric Modes}{41}{subsection.6.4.1}
\contentsline {subsection}{\numberline {6.4.2}Glasses Mode}{41}{subsection.6.4.2}
\contentsline {subsection}{\numberline {6.4.3}C Warn Mode}{41}{subsection.6.4.3}
\contentsline {subsection}{\numberline {6.4.4}Linum Mode}{41}{subsection.6.4.4}
\contentsline {subsection}{\numberline {6.4.5}Viper Mode}{41}{subsection.6.4.5}
\contentsline {subsection}{\numberline {6.4.6}Sub/Super Word Modes}{41}{subsection.6.4.6}
\contentsline {section}{\numberline {6.5}Practice}{41}{section.6.5}
\contentsline {subsection}{\numberline {6.5.1}Macros}{41}{subsection.6.5.1}
\contentsline {subsubsection}{Writing Lines}{41}{section*.29}
\contentsline {subsection}{\numberline {6.5.2}Regular Expressions}{41}{subsection.6.5.2}
\contentsline {subsection}{\numberline {6.5.3}Rectangle Editing}{41}{subsection.6.5.3}
\contentsline {section}{\numberline {6.6}Something Fun}{41}{section.6.6}
\contentsline {subsection}{\numberline {6.6.1}Pong, Tetris, and Snake}{41}{subsection.6.6.1}
\contentsline {chapter}{\numberline {7}Chapter Seven: Running Commands/The Command Line}{44}{chapter.7}
\contentsline {section}{\numberline {7.1}Term}{44}{section.7.1}
\contentsline {section}{\numberline {7.2}Shell}{44}{section.7.2}
\contentsline {section}{\numberline {7.3}Single Commands}{44}{section.7.3}
\contentsline {subsection}{\numberline {7.3.1}\keys {alt+!}}{44}{subsection.7.3.1}
\contentsline {subsection}{\numberline {7.3.2}\keys {alt+{\&}}}{44}{subsection.7.3.2}
\contentsline {subsection}{\numberline {7.3.3}\keys {alt+$\delimiter "026A30C $}}{44}{subsection.7.3.3}
\contentsline {section}{\numberline {7.4}Woman Man Page Reader}{44}{section.7.4}
\contentsline {section}{\numberline {7.5}Something Fun}{44}{section.7.5}
\contentsline {subsection}{\numberline {7.5.1}Eliza}{44}{subsection.7.5.1}
\contentsline {chapter}{\numberline {8}Chapter Eight: Extending Emacs}{45}{chapter.8}
\contentsline {section}{\numberline {8.1}Themes}{45}{section.8.1}
\contentsline {section}{\numberline {8.2}Rebinding Keys}{45}{section.8.2}
\contentsline {section}{\numberline {8.3}Installing Packages/Melpa}{45}{section.8.3}
\contentsline {section}{\numberline {8.4}Taking it Further With Elisp}{45}{section.8.4}
\contentsline {section}{\numberline {8.5}Something Fun}{45}{section.8.5}
\contentsline {subsection}{\numberline {8.5.1}Dunnet}{45}{subsection.8.5.1}
\contentsline {chapter}{\numberline {9}Contributors}{46}{chapter.9}
