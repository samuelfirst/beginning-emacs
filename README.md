# Beginning Emacs

A Beginner's Guide to Emacs

## Index

* [About](https://gitlab.com/samuelfirst/beginning-emacs#about)
* [Building](https://gitlab.com/samuelfirst/beginning-emacs#building)
  * [Dependencies](https://gitlab.com/samuelfirst/beginning-emacs#dependencies)
  * [Building From Source](https://gitlab.com/samuelfirst/beginning-emacs#building-from-source)
* [Contributing](https://gitlab.com/samuelfirst/beginning-emacs#contributing)
  * [Guidelines](https://gitlab.com/samuelfirst/beginning-emacs#guidelines)
  * [Contributors List](https://gitlab.com/samuelfirst/beginning-emacs#contributors-list)
* [License](https://gitlab.com/samuelfirst/beginning-emacs#license)

## About

Beginning Emacs is meant to serve as a first look into using Emacs devoid of
Emacs-isms, such as the use of C/M/S-[char] syntax when describing shortcuts
or the use of meta instead of alt.

## Building

Beginning Emacs comes precompiled in main.pdf, however, if you would like to
compile it yourself, see below.

### Dependencies

* Texlive-full

### Building From Source

1. Clone the repository
   * `git clone https://gitlab.com/samuelfirst/beginning-emacs`
   
2. Cd into the project's directory
   * `cd beginning-emacs`
   
3. Compile to a pdf
   * `pdflatex main.tex && bibtex main.aux && pdflatex main.tex && pdflatex
     main.tex`

4. It should now be compiled to main.pdf

## Contributing

### Guidelines

* Please do not include the compiled pdf in your pull request.
  * The reasoning behind this is that I can't read the diff on a pdf.

* Please do not include aux, out, log, or blg files in your pull request.

* All questions of style or what content should or should not be included come
  down to my personal preference.
  * That being said, I am willing to hear out suggestions, so long as they are
    phrased in a respectful and constructive manner.
	
* If you use any new sources, please include a citation.

### Contributors List

After contributing, if you want, you may add your name (or a nickname) to the
contributors list in contributors.tex.  When adding your name, also put a
short description of your contributions.  If you have already contributed, add
your most recent contributions to your contribution list.  If you are not sure
what to call your contributions, you may instead put misc.

## License

Beginning Emacs is licensed under the GFDL (GNU Free Documentation License).
This means you may copy, modify, and redistribute the work, however, it and
any derivative works must stay licensed under the GFDL.  See the license
section of the document for further details.
