\chapter{Chapter Five: Modes}

\section{What are Modes}
The idea behind modes is that they change how you edit a file.  They do this
by providing a set of commands and keyboard shortcuts, as well as changing how
text is displayed.  This makes Emacs more powerful by allowing it to respond
to the context of what is being edited.

\subsection{Major vs. Minor}
Emacs supports two kinds of modes: major and minor.  Major modes provide lots
of commands and shortcuts and are often tied to a specific kind of file (for
example, when opening a .py file Emacs would use the python major mode).  Only
one major mode can be active per buffer at a time.  Minor modes, on the other
hand make small changes that can be useful across multiple file types.
Multiple minor modes may be active in the same buffer.

\section{Some Useful Modes}

\subsection{Org-mode}
\begin{figure}[H]
  \caption{Org-mode Example}
  \includegraphics[width=1\linewidth]{include/images/org-mode-example}
\end{figure}
No guide to Emacs would be complete without at least mentioning org-mode.
Org, or organizational mode is designed for taking notes, making todo-lists,
and project planning.  What makes org-mode worth mentioning, however, is how
much it is capable of doing while remaining fairly straight-forward.  With
org-mode you can create and fold lists, clock in and out of tasks, easily
create ascii tables, create links to other documents, insert in-line images,
create and run blocks of code with language-specific syntax highlighting,
export your org-file in lots of different formats, and do many other things
that are outside the scope of this book.

\subsubsection{Starting Org-mode}
To start org-mode, either open an org file (a file with the .org extension) or
type \keys{alt+x} and enter the command \textcolor{blue}{org-mode}.

\subsubsection{Lists}
The most basic function of org-mode is the ability to create bulleted,
hierarchical lists.  To create a list item all you have to do is begin a line
with an asterisk (*) followed by a space.  To create sub-items for that item,
use two asterisks followed by a space.  This pattern continues for however
many sub-items you want. \cite{org-mode-lists}

Large numbers of list items and sub-items have a tendency to clutter up the
screen.  Luckily, org-mode has a way to hide sub-items when you don't need
them.  To hide an item's sub-items, move the cursor to the item and press
\keys{tab tab}.  It should now display the item with elipsis to its right and
no sub-items beneath it.  To re-display the sub-items, press \keys{tab}
again.  If you want to fully expand a tree of items and sub-items, press
\keys{tab} twice instead of once. \cite{org-mode-lists}

If you want to use lists as todo lists, org-mode provides keyboard bindings
for marking items as either 'todo' or 'done'.  To cycle between unmarked,
todo, and done, press \keys{shift+$\leftarrow$} or
\keys{shift+$\rightarrow$}.  Similarly, you can add priority (shown as [\#A],
[\#B], and [\#C]) to your tasks with \keys{shift+$\uparrow$} and
\keys{shift+$\downarrow$}. This will be important for the agenda section
below.\cite{org-mode-todo}

\subsubsection{Clocking in and out}
When creating a todo list, it is useful to see how much time you are spending
on each item.  This functionality is provided by org-mode through the ability
to clock in and out of a task.  To clock in, select a list item and press
\keys{ctrl+x+i}.  This will create a new sub-item in the list called
:LOGBOOK:, which contains the time you clocked in.  When you clock out
(\keys{ctrl+x+o}), the time you clocked out and the time spent on the task
will be added to the log.  The logbook can contain multiple
logs. \cite{org-mode-clocking}

\subsubsection{Tables}
While lists are useful for organizing data, some data is better represented in
a tabular format.  Fortunately, tables are one of the features provided by
org-mode.  To create a table, begin the line with $\vert$ (shift + \textbackslash)
followed by a space.  Then enter the name of the first column header followed
by a space, a $\vert$, and another space.  Repeat the previous step for
however many columns are in the table and press \keys{tab}.  There should now
be a second row below the first.  You can move between the cells by pressing
\keys{tab} and \keys{shift+tab}.  Tabbing forward from the last cell in the
table creates a new row.  If you want to insert a horizontal line, move the
cursor to the first position in the first cell of the row, enter a dash, and
press \keys{tab}. \cite{org-mode-tables}

\subsubsection{Hyperlinks}
Org-mode also provides the ability to link to other files, websites,
etc. through hyperlinking.  To insert a hyperlink, type \keys{ctrl+c+l}.
You will now be prompted for the link type.  In order to see the possible link
types, press \keys{tab}, then enter the type of link you want to insert (if
you want to link to a file, for example, enter \textcolor{blue}{file}) and
press \keys{enter}.  You will then be prompted for a description.  This is the
text that will be shown as the link.  Once you have entered the description,
press \keys{enter} and a link to the file will be inserted.  If you want to
edit the link, move the cursor over it and type \keys{ctrl+c+l} again.  If you
want to open the link, either click it or move the cursor over it and type
\keys{ctrl+c+o}. \cite{org-mode-links}

\subsubsection{In-Line Images}
When linking to image files, if you would like to display them in the buffer,
type \keys{ctrl+c+x+v}.  To hide the images, type \keys{ctrl+c+x+v} again.  If
you would like to caption the images, type '\#+CAPTION: caption text' above
the link. \cite{org-mode-inline}

\subsubsection{Code Blocks}
One of the features of org-mode is the ability to insert syntax-highlighted,
runnable blocks of source code.  This section presumes some programming
knowledge, so if you don't have any, you may skip it for now (as well as the
practice related to programming).
\begin{list}{\labelitemi}{\leftmargin=1em}
\item\textbf{Creating and Editing Blocks:}
  To create a code block, type '\#+BEGIN\_SRC $<$language$>$
  ', where language is your programming language of choice.  Then, on the next
  line, type '\#+END\_SRC'.  You can then insert code into the codeblock by
  moving the cursor into it and pressing \keys{ctrl+c} \keys{'}.  When you are done
  editing the code, press \keys{ctrl+c} \keys{'} again to exit.  To modify the
  code in the block, just re-enter \keys{ctrl+c}
  \keys{'}. \cite{org-mode-code-blocks}
\item\textbf{Supported Languages:}
  Below is a table of supported languages and their assosciated identifer
  \begin{table}[H]
    \caption{Supported Languages\cite{org-mode-code-blocks-languages}}
    \centering
    \begin{tabular}{| l | l |}
      \hline
      \multicolumn{1}{| c |}{\textbf{Language}} &
      \multicolumn{1}{c |}{\textbf{Identifier}} \\
      \hline
      Asymptote     & asymptote  \\
      Awk           & awk        \\
      C             & C          \\
      C++           & C++        \\
      Clojure       & clojure    \\
      CSS           & css        \\
      D             & D          \\
      Ditaa         & ditaa      \\
      Emacs Calc    & calc       \\
      Emacs Lisp    & emacs-lisp \\
      Fortran       & fortran    \\
      Gnuplot       & gnuplot    \\
      Gnu Screen    & screen     \\
      Graphviz      & dot        \\
      Haskell       & haskell    \\
      Java          & java       \\
      Javascript    & js         \\
      LaTeX         & latex      \\
      Ledger        & ledger     \\
      Lilypond      & lilypond   \\
      Lisp          & lisp       \\
      Lua           & lua        \\
      MATLAB        & matlab     \\
      Mscgen        & mscgen     \\
      OCaml         & ocaml      \\
      Octave        & octave     \\
      Org mode      & org        \\
      Oz            & oz         \\
      Perl          & perl       \\
      Plantuml      & plantuml   \\
      Processing.js & processing \\
      Python        & python     \\
      R             & R          \\
      Ruby          & ruby       \\
      Sass          & sass       \\
      Scheme        & scheme     \\
      Sed           & sed        \\
      Shell         & sh         \\
      SQL           & sql        \\
      SQLite        & sqlite     \\
      Vala          & vala       \\
      \hline
    \end{tabular}
  \end{table}
\item\textbf{Running Your Code:}
  By default, org will only execute code written in Emacs Lisp.  To enable the
  execution of other programming languages, add the code snippet below to your
  .emacs file (\keys{ctrl+x+f} \textcolor{blue}{$\sim$/.emacs}), replacing
  $<$your-language$>$  with your language of choice.  To
  enable more than one language, just add it another list (the (language . t)
  part).  To disable a language, change 't' to 'nil'.  Once you have enabled
  your language of choice, you can execute the code by pressing
  \keys{ctrl+c+c}.  You should see a prompt in the mini-buffer saying
  \textcolor{olive}{Evaluate this python code block on your system (yes or
    no)}.  Type \textcolor{blue}{yes} and press enter to run your code.  The
  results will be output in a \#+RESULTS block below the code block.  If they
  are taking up too much room, both the results block and the code block can
  be folded by pressing \keys{tab}. \cite{org-mode-code-blocks}

\begin{figure}[H]
\caption{Enabling Languages\cite{org-mode-code-blocks-languages}}
\begin{verbatim}
(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (<your-language> . t)))
\end{verbatim}
\end{figure}

\item\textbf{Naming Your Block:}
  If you want to name your block of code so it can be referenced by other
  blocks later (we will cover how to do this in the 'Passing Variables'
  section), you can add '\#+NAME: $<$name$>$ ' above your block.  A
  side benefit of this is that the output of the block will be attached to
  that block and will not be overwritten by the output of other
  blocks. \cite{org-mode-code-blocks}

\item\textbf{Headers:}
  Various arguments can be passed to a code-block to change how it runs.
  These arguments are placed in a header section (\#+HEADER), like this:
  \#+HEADER: :$<$argument-name$>$ $<$argument-value$>$.  To, for example, have
  the header place anything printed by the block in the results section, you
  would use the header \#+HEADER: :results output. \cite{org-mode-code-blocks}

\item\textbf{Defining and Passing Variables:}
  If you want to use a code-block to interpret some data somewhere else in
  the buffer, you can pass it to the code-block by placing the ':var' flag in
  the code-block's header, like this: ':var $<$block-name$>$=$<$org-name$>$',
  substituting $<$org-name$>$ for the value in the item's \#+NAME, and the
  block-name for whatever you want to call the variable in the
  block. \cite{org-mode-code-blocks}

  If, for example, if you had a document with a table that you wanted to
  run through, you would put '\#+NAME: my-table' above the table, and
  '\#+HEADER: :var table=my-table :hlines no' above the code block.  The table
  would then be accessible in the code block as a list of lists.  The purpose
  of the ':hlines no' above is to strip out horizontal
  separators.  Below is an example, using python. \cite{org-mode-code-blocks}

\begin{figure}[H]
\caption{Org-mode variable passing example}
\begin{verbatim}
#+NAME: my-table
| 1 | 2 | 3 |
|---+---+---|
| 4 | 5 | 6 |
|---+---+---|
| 7 | 8 | 9 |

#+NAME: test-block
#+HEADER: :var table=my-table :hlines no :results output
#+BEGIN_SRC python
for row in table:
    for item in row:
        print(str(item))
#+END_SRC
\end{verbatim}
\end{figure}

  It should be noted that bulleted list items can not be referenced from
  within code-blocks, however, their contents can.

\item\textbf{Passing Code Blocks as Variables:}
  In addition to being able to pass parts of the buffer as variables, you can
  also pass code-blocks as variables.  This works similarly to calling the
  block being passed as a function.  At run-time, you will be prompted to run
  each block being referenced.  The output of each block is then put into its
  assigned variable, which is passed to the main block.  This allows for
  multiple languages to be chained together. \cite{org-mode-code-blocks}

\begin{figure}[H]
\caption{Org-mode code-block passing example}
\begin{verbatim}
#+NAME: block-a
#+BEGIN_SRC python
return("Hello World!")
#+END_SRC

#+NAME: block-b
#+HEADER: :var hello=block-a :results output
#+BEGIN_SRC python
print(hello)
#+END_SRC
\end{verbatim}
\end{figure}
\end{list}

\subsubsection{Agenda}
If you want to use org-mode as a weekly planner, it has that functionality
built in with org-agenda.  The agenda allows you to schedule events across
multiple org-files and view them in one central place.  To use the agenda,
begin by adding the files you want to it with the shortcut \keys{ctrl+c}
\keys{[} or the command \textcolor{blue}{org-agenda-file-to-front}.  You can
then schedule a task by moving the cursor to the task and pressing
\keys{ctrl+c+s}.  This will open a buffer with a calendar with the current
date selected.  To change the date, press \keys{shift+$\uparrow$},
\keys{$\downarrow$}, \keys{$\leftarrow$}, or \keys{$\rightarrow$}.  To
view the tasks you have scheduled, open the org-agenda with the command
\textcolor{blue}{org-agenda}.  This will bring up a list of commands.  Type
'a' to view the tasks for the current week.  Pressing tab will jump to the
task under the cursor if its assosciated org-file is
open. \cite{org-mode-agenda}

Files added to the agenda will stay there until removed.  To remove files from
the agenda use either the shortcut \keys{ctrl+c} \keys{]}, or the command
\textcolor{blue}{org-remove-file}. \cite{org-mode-agenda}

\subsubsection{Exporting}
To export your org-file to a different format, type \keys{ctrl+c+e}.  This
will open up the export menu.  At the top of the menu are various toggleable
options to change what is exported and how it is exported.  You can see the
table below for more information on what they do.  Below the toggles are the
various export formats.  Find the one you want and type the bracketed
character to its left.  You should now be in the sub-menu below the option
you chose.  Find the option that is the best for you and type the bracketed
character to its left.  The file should now be
exported. \cite{org-mode-exporting}

\begin{table}[H]
    \caption{Org-export Toggles\cite{org-mode-exporting}}
    \centering
    \begin{tabular}{| l | l |}
      \hline
      \multicolumn{1}{| c |}{\textbf{Toggle}} &
      \multicolumn{1}{c |}{\textbf{What it does}} \\
      \hline
      Body Only        & Excludes headers and footers from the export \\
      Export Scope     & Only export subtree beginning at the cursor  \\
      Async Export     & Export file in the background                \\
      Visible Only     & Do not export folded items                   \\
      Force Publishing & Forces export, even if files are unchanged   \\
      \hline
    \end{tabular}
\end{table}

\subsection{Scheduling}
\begin{figure}[H]
  \caption{Diary/Calendar Example\cite{star-trek-man-trap}}
  \includegraphics[width=1\linewidth]{include/images/diary-calendar-example}
\end{figure}
Emacs provides support for planning outside of org-mode with the calendar and
diary modes.

\subsubsection{Calendar}
Emacs comes with a built-in calendar, which can be used to look up holidays,
calculate the number of days between dates, and display entries from
diary-mode.

To start calendar-mode, press \keys{alt+x} and type
\textcolor{blue}{calendar}.  There should now be a small buffer open with the
current month as well as the previous and next months.  You can use the
arrow keys to change the selected date.  To view holidays on a selected day,
type \keys{h}.  If there is a holiday on that date, it will be displayed in
the mini-buffer, like this: \textcolor{olive}{Thursday, February 14, 2019:
  Valentine's Day}.  To view all the holidays in the displayed months, type
\keys{a}.  To view the diary entries for a day, press \keys{d} (the diary is
covered in the next section).  If you want to count the days between two
dates, begin by moving the cursor to the first date and typing
\keys{ctrl+space}.  Then move the cursor to the second date and type
\keys{alt+=}.  The number of days between will be printed in the mini-buffer.

\subsubsection{Diary}
Diary mode allows you to log events as they happen in a central diary file,
schedule recurring future events, and plan for anniversaries and birthdays.
To use diary-mode, you create the file $\sim$/.emacs.d/diary.  To do this, type
\keys{ctrl+x+f} \textcolor{blue}{$\sim$/.emacs.d/diary}.  This will
create the file and start diary-mode.  When you want to add a new entry to
your diary, just reopen the file.

You can now create entries in the diary by beginning a line with the date in
either the short (numerical) 'month/day/year' format (ex. 6/25/2000), or the
long (month written out) 'month day, year' format (ex. June 25, 2000).  You
can also include a time after the date, like this: 6/25/2000 10:00.  If you
would like to include several time-entries under one date, simply put one or
more spaces before them, like the figure below.\cite{emacs-wiki-diary-mode}

\begin{figure}[H]
\caption{Diary Multiple Entries Example}
\begin{verbatim}
April 10, 1912
    9:30 Boarded Ship
    12:00 Met penniless artist

April 12, 1912
    14:00 Got painted
    15:00 Ruined automobile
    15:01 Broke up with fiance
    15:01 "Lost" necklace

April 14, 1912
    10:00 Artist arrested
    10:30 Artist escaped
    11:00 Ship hit iceberg
    11:10 Got in life boat
    11:15 Got out of life boat
    11:20 Boat sank; floating on door
    12:00 Artist died
    12:05 Got picked up by new ship

March 22, 1996
    12:00 Dropped necklace into ocean; had it all along
\end{verbatim}
\end{figure}

To schedule a recurring weekly event, rather than entering a date, you can
enter the day of the week it occurs on.  For example, if you need to make a
phone call every monday at 11:00 , you would add the line 'Monday 11:00 Make
Call' to your diary file.\cite{emacs-wiki-diary-mode}

To schedule a recurring yearly event, like an anniversary, add a line in this
format: '\%\%(diary-anniversary [month] [day] [year]) [event]'  If you want to
include the number of years since the anniversary started, you can do that with
\%d (ex. \%\%(diary-anniversary 2 13 2010) Yearly pre-Valentines scramble; \%d
years running).\cite{emacs-wiki-diary-mode}

If you want to schedule an entire block of days at once, you can do that with
'\%\%(diary-block [start-month] [start-day] [start-year] [end-month] [end-day]
[end-year]) [event]'.\cite{emacs-wiki-diary-mode}

You can also schedule an event to repeat every couple of days with
'\%\%(diary-cyclic [days-until-repetition] [day] [month] [year]) [event]'.
For example, if you wanted to repeat an event every three days starting with
July 1, 2015, you would add the line '\%\%(diary-cyclic 3 7 1 2015) My Event'
to your diary file.\cite{emacs-wiki-diary-mode}

\subsubsection{Org-mode Integration}
If you would like to combine the features of org-mode with the calendar and
agenda, add the line (setq org-agenda-include-diary t) to your $\sim$/.emacs
file.  Once you have done this, you can view diary entries in the org-mode
agenda.  The diary entries can be jumped to the same way you would jump to a
task (\keys{tab}).  To insert a new diary entry, type \keys{i}.  If you want
to jump from the agenda to the calendar, press \keys{c} (to jump back, just
press it again).\cite{org-mode-agenda-diary}

\subsection{Image Mode}
In graphical mode, Emacs has the ability to display images.  To use emacs as
an image viewer, just open an image file.  If you open an image file in dired
mode (started by opening a directory), you can use \keys{n} and \keys{p} to
view the next and previous images respectively.  When viewing an animated gif,
\keys{enter} will play/pause the animation, \keys{f} will jump to the next
frame, \keys{b} will jump to the previous frame, and \keys{a r} will reverse
the animation.

\section{Practice}

\subsection{Org Basics}
Create a new org-file named todo.org and use it to create a todo list for the
next (few) hour(s).  Mark each task as TODO, then, once completed, DONE.
Clock into and out of tasks as you work on them.

\subsubsection{Bonus: Programming in org}
The file beginning-emacs/include/chapter\_five/spreadsheet.org contains a
table with the names, positions, and salaries of the employees of the FooBar
company.  Write code in your language of choice to produce a list of employees
sorted by salary (descending order), return a list of sales assosciates, and
sort employees by name alphabetically.

\subsection{Remember, Remember}
Use the diary to record important dates (birthdays, anniversaries, etc.).
Then, in calendar mode, use \keys{o} to jump to those dates and view the
events you recorded.  Bonus points for including the number of years since the
event started.

\subsection{Bonus: Researching Other Modes}
This chapter only covered a small subsection of the modes that are available
in Emacs.  Use the webpage
\url{https://www.emacswiki.org/emacs/CategoryModes}\footnotemark[1] to
research other modes.

\section{Something Fun}
Emacs has the ability to apply different effects to text in a
screensaver-esque fashion with zone-mode.  When activated, this mode will
create a copy of the text in the current buffer and apply a random transition
to it.  It might appear that it is destroying the file at first, but don't
panic, it keeps the original file intact. 

\subsection{Zone Mode}
\begin{list}{\labelitemi}{\leftmargin=1em}
\item Step 1. Type \keys{alt+x}
\item Step 2. Type \textcolor{blue}{zone}
\item Step 3. Press any key to exit zone mode
\end{list}

\footnotetext[0]{All shortcuts and commands can be found in the Emacs
  manual\cite{Emacs-Manual}}

\footnotetext[1]{\href{https://web.archive.org/web/20160818000152/https://www.emacswiki.org/emacs/CategoryModes}{Archived
    version of https://www.emacswiki.org/emacs/CategoryModes}}
